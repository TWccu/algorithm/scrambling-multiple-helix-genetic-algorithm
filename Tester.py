import copy
import datetime
import itertools
import math
import random as rd

import numpy as np
from matplotlib import pyplot as plt

from Route import Route
from RoutesGroup import RoutesGroup
from ScrambleProcessor import ScrambleProcessor
from World import World


def create_random_position(num=10):
    X = []
    for i in range(num):
        x = 6 * rd.random()
        y = 6 * rd.random()
        X.append([x, y])

    return X


def get_distance_table(X):
    distance_table = [[999999 for i in range(len(X))] for j in range(len(X))]
    for i in range(len(X)):
        for j in range(i + 1, len(X)):
            distance = round(get_square_distance(X[i], X[j]), 1)
            distance_table[i][j] = distance
            distance_table[j][i] = distance
    return distance_table


def get_square_distance(position1, position2):
    x_2 = (position1[0] - position2[0]) ** 2
    y_2 = (position1[1] - position2[1]) ** 2
    result = math.sqrt(x_2 + y_2)
    return result


# [[t1, t3, t6], [t2, t5, t4], [t7, t10, t8, t9]]
def get_assign_sequences(Y, agv_num):
    S = [[] for _ in range(agv_num)]
    for i in range(len(Y)):
        S[Y[i]].append(i)
    return S


def get_shortest_target(agvs, agvId, targets, assign_sequence):
    humanId = -1
    distance = 999999
    agv = agvs[agvId]
    for target in assign_sequence:
        start = [targets[target][0], targets[target][1]]
        _dis = get_square_distance(agv, start)

        if _dis < distance:
            distance = _dis
            humanId = target

    return humanId


def sort_assign_sequence_by_near_path(assign_sequences, cost_map, targets, startpts):
    sorted_assign_sequence = assign_sequences[:][:]
    for i in range(len(assign_sequences)):
        ase = assign_sequences[i][:]
        first_node = get_shortest_target(startpts, i, targets, ase)
        if first_node == -1:
            continue
        path = [ase.pop(ase.index(first_node))]
        for j in range(len(assign_sequences[i]) - 1):
            min_distance = 999999
            next_node = -1
            for k in ase:
                if cost_map[path[j]][k] < min_distance:
                    min_distance = cost_map[path[j]][k]
                    next_node = k
            path.append(next_node)
            ase.pop(ase.index(next_node))
        sorted_assign_sequence[i] = path

    return sorted_assign_sequence


def draw_path(routes_group, init_group_node, group_color_tlist, target_table, target_c):
    for i in range(len(routes_group)):
        # fig, ax = plt.subplots()
        xs = [init_group_node[i][0]]
        ys = [init_group_node[i][1]]
        for j in routes_group[i]:
            xs.append(target_table[j][0])
            ys.append(target_table[j][1])
        plt.plot(xs, ys, group_color_tlist[i], linestyle='-', marker='')
    for i in range(len(target_table)):
        plt.text(target_table[i, 0], target_table[i, 1], i, fontsize=9)

    plt.scatter(target_table[:, 0], target_table[:, 1], c=target_c, s=50, cmap='viridis')
    plt.scatter(init_group_node[:, 0], init_group_node[:, 1], c=group_color_tlist, s=300, alpha=0.6)

    plt.show()


def cluster_by_customized_kmeans(data_X, original_centroids, max_iter=50, use_kmeans_pluspluse=True):
    centroids = copy.deepcopy(original_centroids)
    Y = [0] * len(data_X)
    for i in range(max_iter):
        for index1, value in enumerate(data_X):
            min_distance = [0, 9999999]  # [index, value]
            for index2, centroid in enumerate(centroids):
                #                 distance = get_distance(value, centroid)
                distance = get_square_distance(value, centroid)
                if distance <= min_distance[1]:
                    min_distance = [index2, distance]
            Y[index1] = min_distance[0]
        ## update centroids
        for i in range(len(centroids)):
            x = 0
            y = 0
            num = 0
            for index1, value in enumerate(data_X):
                if Y[index1] == i:
                    x = x + value[0]
                    y = y + value[1]
                    num += 1
            if num is 0:
                if use_kmeans_pluspluse:
                    max_distance = [0, 0]
                    for index1, value in enumerate(data_X):
                        #                     distance = get_distance(centroids[i], value)
                        distance = get_square_distance(centroids[i], value)
                        if distance >= max_distance[1]:
                            max_distance = [index1, distance]
                    centroids[i] = data_X[max_distance[0]]
            else:
                centroids[i][0] = x / num
                centroids[i][1] = y / num

    return Y, centroids


def update_assign_sequence(Y, route_assign_agv):
    routes = [raa.path for raa in route_assign_agv]
    for i, nodes in enumerate(routes):
        for j in nodes:
            Y[j] = i
    return Y


def optimal_by_swap_belong_agv(agvs_position, targets_position, routes_group, longest_score):
    original_assign_sequence = [i for i in range(0, len(agvs_position))]
    all_assign_sequence_combinations = itertools.permutations(original_assign_sequence)
    # print(len(all_assign_sequence_combinations))
    best_assign_sequence = original_assign_sequence[:]
    original_route_assign_agv = routes_group[:]
    current_score = sum([route.get_score() for route in original_route_assign_agv])
    longest_score = longest_score
    new_longest_score = longest_score
    # print("current_score: " + str(current_score))
    for operate_assign_sequence in all_assign_sequence_combinations:
        new_score = 0
        for index, k in enumerate(operate_assign_sequence):
            if len(original_route_assign_agv[index].path) == 0:  # bug
                init_score = 0
            else:
                init_score = get_square_distance(agvs_position[k],
                                                 targets_position[original_route_assign_agv[index].path[0]])
            score = init_score + original_route_assign_agv[index].get_inner_score()
            new_longest_score = min(longest_score, score)
            new_score += score
            # print(original_route_assign_agv[index].get_inner_score())
        if new_score < current_score and new_longest_score < longest_score:
            print("swap_belong_agv")
            best_assign_sequence = operate_assign_sequence
            current_score = new_score
    best_route_assign_agv = original_route_assign_agv[:]
    for i, gid in enumerate(best_assign_sequence):
        best_route_assign_agv[i] = original_route_assign_agv[gid]
    return best_route_assign_agv


target_num = 2
level_num = min(20, int(target_num * 1.5))
populations_num = min(50, int(target_num * 1.5))
targets = create_random_position(num=target_num)
targets = np.array(targets)
# print(targets)
plt.scatter(targets[:, 0], targets[:, 1])
distance_table = get_distance_table(targets)
# distance_table = np.array(distance_table)

startpts = np.array([[-1.0, 0.0], [-0.5, -0.5], [0.0, -1.0]])
# startpts = np.array([[-1.0, 0.0], [-0.5, -0.5]])
startpts_origin = copy.deepcopy(startpts)

startpts_c = ['purple', 'green', 'red']
# startpts_c = ['purple', 'green']
plt.scatter(targets[:, 0], targets[:, 1])
plt.scatter(startpts[:, 0], startpts[:, 1], c=startpts_c, s=300, alpha=0.6)
plt.show()

(Y, centroids) = cluster_by_customized_kmeans(targets, startpts, max_iter=50, use_kmeans_pluspluse=True)

Y_ = [''] * len(Y)
for k, value in enumerate(Y):
    Y_[k] = startpts_c[value]
plt.scatter(targets[:, 0], targets[:, 1], c=Y_, s=50, cmap='viridis')
# plt.scatter(centroids[:, 0], centroids[:, 1], c=startpts_c, s=300, alpha=0.6)
plt.show()

assign_sequences = get_assign_sequences(Y, len(startpts))
sorted_assign_sequence = sort_assign_sequence_by_near_path(assign_sequences, distance_table, targets, startpts_origin)
# print(sorted_assign_sequence)
# draw_path(sorted_assign_sequence, startpts_origin, startpts_c, targets, Y_)

route_group_list = []
for index, value in enumerate(sorted_assign_sequence):
    if len(value) == 0:
        init_score = 0
    else:
        init_score = get_square_distance(startpts[index], targets[value[0]])
    route = Route(value, distance_table, init_score)
    route_group = RoutesGroup(index, route, distance_table, level=level_num, populations=populations_num, variant=0.3,
                              mutate_percent=0.3, elite_save_percent=0.1)
    route_group_list.append(route_group)

world = World(Y, route_group_list, distance_table, avgs_position=startpts, nodes_position=targets, variation_max_time=5,
              group_evolute_level=level_num, use_multiple_process=False)

print("Original score: %f" % world.get_score())

start_time = datetime.datetime.now()
optimal_assign_sequence = []
longest_time = 0

time = 1
for score in world.evolute():
    longest_time = score
    print("World, time %d : %f" % (time, score))
    time += 1
    optimal_assign_sequence = []
    for a, route_group in enumerate(world.routes_group):
        optimal_assign_sequence.append(route_group.routes[0])
    draw_path([route.path for route in optimal_assign_sequence], startpts_origin, startpts_c, targets, Y_)
print(world.get_all_group_best_score())

optimal_assign_sequence = []
for a, route_group in enumerate(world.routes_group):
    optimal_assign_sequence.append(route_group.routes[0])

best_route_assign_agv = optimal_by_swap_belong_agv(startpts, targets, optimal_assign_sequence, longest_time)

Y = update_assign_sequence(Y_, best_route_assign_agv)
Y_ = [''] * len(Y)
for k, value in enumerate(Y):
    Y_[k] = startpts_c[value]
draw_path([route.path for route in best_route_assign_agv], startpts_origin, startpts_c, targets, Y_)

scramble_processor = ScrambleProcessor(Y, startpts, targets, cost_map=distance_table,
                                       best_routes_group=best_route_assign_agv, top_num=3)

end_time = datetime.datetime.now()
print((end_time - start_time).microseconds)

time = 1
start_time = datetime.datetime.now()
scramble_processor.scramble()
for score in scramble_processor.scramble():
    longest_time = score
    print("scramble_processor, time %d : %f" % (time, score))
    time += 1
    optimal_assign_sequence = []
    for a, route_group in enumerate(scramble_processor.best_routes_group):
        optimal_assign_sequence.append(route_group)
    Y = scramble_processor.cluster_sequence
    Y_ = [''] * len(Y)
    for k, value in enumerate(Y):
        Y_[k] = startpts_c[value]
    draw_path([route.path for route in optimal_assign_sequence], startpts_origin, startpts_c, targets,
              Y_)
print(scramble_processor.get_all_group_best_score())

end_time = datetime.datetime.now()
print((end_time - start_time).microseconds)

# optimal_assign_sequence = []
# for a, route_group in enumerate(scramble_processor.best_routes_group):
#     optimal_assign_sequence.append(route_group)
# Y = scramble_processor.cluster_sequence
# Y_ = [''] * len(Y)
# for k, value in enumerate(Y):
#     Y_[k] = startpts_c[value]
# draw_path([route.path for route in optimal_assign_sequence], startpts_origin, startpts_c, targets, Y_)
