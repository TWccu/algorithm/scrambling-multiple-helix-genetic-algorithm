import math

import numpy as np
from matplotlib import pyplot as plt


def create_random_position(num=10):
    X = []
    for i in range(num):
        x = 6 * np.random.uniform()
        y = 6 * np.random.uniform()
        X.append([x, y])

    return X


def get_distance(position1, position2):
    x_2 = (position1[0] - position2[0]) ** 2
    y_2 = (position1[1] - position2[1]) ** 2
    result = math.sqrt(x_2 + y_2)
    return result


def get_distance_table(X):
    distance_table = [[0 for i in range(len(X))] for j in range(len(X))]
    for i in range(len(X)):
        for j in range(i + 1, len(X)):
            distance = round(get_distance(X[i], X[j]), 1)
            distance_table[i][j] = distance
            distance_table[j][i] = distance
    return distance_table


def get_square_distance(position1, position2):
    x_2 = (position1[0] - position2[0]) ** 2
    y_2 = (position1[1] - position2[1]) ** 2
    result = math.sqrt(x_2 + y_2)
    return result


def cluster_by_customized_kmeans(data_X, centroids, max_iter=50, use_kmeans_pluspluse=True):
    Y = [0] * len(data_X)
    for i in range(max_iter):
        for index1, value in enumerate(data_X):
            min_distance = [0, 9999999]  # [index, value]
            for index2, centroid in enumerate(centroids):
                #                 distance = get_distance(value, centroid)
                distance = get_square_distance(value, centroid)
                if distance <= min_distance[1]:
                    min_distance = [index2, distance]
            Y[index1] = min_distance[0]
        ## update centroids
        for i in range(len(centroids)):
            x = 0
            y = 0
            num = 0
            for index1, value in enumerate(data_X):
                if Y[index1] == i:
                    x = x + value[0]
                    y = y + value[1]
                    num += 1
            if num is 0:
                if use_kmeans_pluspluse:
                    max_distance = [0, 0]
                    for index1, value in enumerate(data_X):
                        #                     distance = get_distance(centroids[i], value)
                        distance = get_square_distance(centroids[i], value)
                        if distance >= max_distance[1]:
                            max_distance = [index1, distance]
                    centroids[i] = data_X[max_distance[0]]
            else:
                centroids[i][0] = x / num
                centroids[i][1] = y / num

    return Y, centroids


targets = create_random_position(num=100)
targets = np.array(targets)
# print(targets)
plt.scatter(targets[:, 0], targets[:, 1])
distance_table = get_distance_table(targets)
distance_table = np.array(distance_table)

startpts = np.array([[-3.0, -4.0], [-4.0, -3.0], [-8.0, -8.0]])
# print(hasattr(startpts, '__array__'))
startpts_c = ['purple', 'yellow', 'green']
plt.scatter(targets[:, 0], targets[:, 1])
plt.scatter(startpts[:, 0], startpts[:, 1], c=startpts_c, s=300, alpha=0.6)
plt.show()

(Y, centroids) = cluster_by_customized_kmeans(targets, startpts, max_iter=50, use_kmeans_pluspluse=True)

Y_ = [''] * len(Y)
for k, value in enumerate(Y):
    Y_[k] = startpts_c[value]
plt.scatter(targets[:, 0], targets[:, 1], c=Y_, s=50, cmap='viridis')
plt.scatter(centroids[:, 0], centroids[:, 1], c=startpts_c, s=300, alpha=0.6)
plt.show()
