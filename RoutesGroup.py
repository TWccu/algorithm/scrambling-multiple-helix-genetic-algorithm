#!/usr/bin/env python
import math
import random as rd

from Route import Route


class RoutesGroup:

    def __init__(self, agv_id, first_route: Route, cost_map, level=10, populations=20, variant=0.3, mutate_percent=0.01,
                 elite_save_percent=0.1):
        self.variation_counter = 5
        self.cost_map = cost_map
        self.agv_id = agv_id
        self.routes = [first_route]
        self.level = level
        self.variant = int(len(first_route.path)*variant)
        self.populations = populations
        self.mutates = int(populations * mutate_percent)
        self.elite = max(2, int(populations * elite_save_percent))
        self.best_score = first_route.score
        # initial and sort populations
        self._generate_random_routes(self.populations)

    def get_best_score(self):
        return self.routes[0].get_score()

    def _sort_routes(self):
        self.routes = sorted(self.routes, key=lambda route: route.score)

    def get_populations_num(self):
        return len(self.routes)

    def keep_routes(self, num):
        if num <= 0:
            raise ValueError("The num of keep routes cannot be less then 0")
        else:
            self.routes = self.routes[:num]

    def evolution(self, variation_max_time):
        # s = datetime.datetime.now()

        if len(self.routes[0].path) < 3:
            return self.routes
        if self.variation_counter < 1:
            return self.routes
        else:
            for _ in range(self.level):
                self.routes = self._get_next_level()
            self._sort_routes()
            score = self.get_best_score()
            if score == self.best_score:
                self.variation_counter -= 1
            else:
                self.variation_counter = variation_max_time
                self.best_score = score
        # e = datetime.datetime.now()
        # print((e - s).microseconds)

        return self.routes

    def evolution_by_multiple_process(self, conn, variation_max_time):
        # s = datetime.datetime.now()
        self.evolution(variation_max_time)
        conn.send(self.routes)
        conn.close()
        # e = datetime.datetime.now()
        # print((e - s).microseconds)

    def _get_next_level(self):
        self._sort_routes()
        elites = self.routes[:self.elite][:]
        new_population = self._crossover_and_mutate(elites)
        return new_population[:] + elites

    def get_closest_node(self, node):
        distance = 999999
        closest_node = -1
        for value in self.routes[0].path:
            if value == node:
                continue
            if self.cost_map[node][value] < distance:
                distance = self.cost_map[node][value]
                closest_node = value
        return closest_node

    def sort_closest_node(self, node):
        temp_cost = self.cost_map[node][:]
        temp_cost = [int(cost * 50) for cost in temp_cost]
        temp_cost[node] = -1
        max_cost = max(temp_cost)
        temp_cost[node] = max_cost + 1
        bucket = [0] * (max_cost + 2)
        for value in self.routes[0].path:
            cost = temp_cost[value]
            bucket[cost] = value
        sorted_node_sequence = []
        for i in bucket:
            if i > 0:
                sorted_node_sequence.append(i)
        return sorted_node_sequence

    def _crossover_and_mutate(self, elites):
        normal_breeds = []
        mutate_ones = []
        if len(self.routes[0].path) < 3:
            return
        for _ in range(self.populations - self.mutates):
            father, mother = rd.choices(elites[:], k=2)
            index_start = rd.randrange(1, len(father.path) - self.variant)
            father_gene = father.path[index_start: index_start + self.variant]
            mother_gene = [gene for gene in mother.path if gene not in father_gene]
            mother_gene_cut = rd.randrange(1, len(mother_gene))
            new_path = mother_gene[:mother_gene_cut] + father_gene + mother_gene[mother_gene_cut:]
            new_route = Route(new_path, cost_map=self.cost_map, init_score=self.routes[0].init_score)
            normal_breeds.append(new_route)

            # mutate 1. 兩點置換 2. 兩點置換，並反轉之間路徑 3. 隨機點跳入最近點
            mutate_path = father.path[:]
            index = range(1, len(mutate_path))
            gene1, gene2 = rd.sample(index, 2)
            strategy = rd.random()  # 使第三strategy不運作
            if strategy < 0.3:
                mutate_path[gene1], mutate_path[gene2] = mutate_path[gene2], mutate_path[gene1]
            elif strategy < 1:
                mutate_path[gene1: gene2 + 1] = mutate_path[gene1: gene2 + 1][::-1]
            else:
                node = mutate_path[gene1]
                closest_node = self.get_closest_node(node)
                closest_node_index = mutate_path.index(closest_node)
                mutate_path.pop(gene1)
                mutate_path.insert(closest_node_index, node)

            mutate_gene = Route(mutate_path, cost_map=self.cost_map, init_score=self.routes[0].init_score)
            mutate_ones.append(mutate_gene)
        mutate_breeds = rd.choices(mutate_ones, k=self.mutates)

        return normal_breeds + mutate_breeds

    def _generate_random_routes(self, num):
        if len(self.routes[0].path) < 1:
            return
        for _ in range(num - 1):
            nodes = self.routes[0].path[1:]
            path = [self.routes[0].path[0]]
            while nodes:
                next_node = nodes.pop(nodes.index(rd.choice(nodes)))
                path.append(next_node)
            route = Route(path, self.cost_map, self.routes[0].init_score)
            self.routes.append(route)
        # self.routes.pop(0)

    @staticmethod
    def _get_square_distance(position1, position2):
        x_2 = (position1[0] - position2[0]) ** 2
        y_2 = (position1[1] - position2[1]) ** 2
        result = math.sqrt(x_2 + y_2)
        return result

    def optimal_by_swap_all_node(self, avgs_position, nodes_position):
        best_route = self.routes[0]
        old_path = best_route.path[:]
        if len(old_path) < 2:
            return
        # print(old_path)
        best_score = best_route.get_score()
        new_path = old_path[:]
        for node in old_path:
            path_calculator_b = Route(old_path, best_route.cost_map, best_route.init_score)
            path_calculator_f = Route(old_path, best_route.cost_map, best_route.init_score)
            opt_path = new_path[:]
            closest_node_sequence = self.sort_closest_node(node)
            max_range = min(3, len(closest_node_sequence))

            for i in range(max_range):
                temp_path_f = new_path[:]
                temp_path_b = new_path[:]
                closest_node = closest_node_sequence[i]
                i = new_path.index(node)
                temp_path_b.pop(i)
                temp_path_f.pop(i)
                closest_node_index = new_path.index(closest_node)
                temp_path_b.insert(closest_node_index + 1, node)
                temp_path_f.insert(closest_node_index, node)
                # path_b score
                path_calculator_b.path = temp_path_b
                path_b_score = path_calculator_b.get_score()
                # path_f score
                path_calculator_f.path = temp_path_f
                # 檢查起點是否被替換，需要更新initial_score
                if closest_node_index == 0:
                    # print("Replace begin node!")
                    distance = self._get_square_distance(avgs_position[self.agv_id], nodes_position[node])
                    path_calculator_f.init_score = distance
                path_f_score = path_calculator_f.get_score()
                if path_b_score < best_score:
                    # print(path_b_score)
                    # print(best_score)
                    # print("Got it! B " + str(self.agv_id) + " ,node: " + str(node) + " ,to: " + str(closest_node))
                    best_score = path_b_score
                    opt_path = temp_path_b
                if path_f_score < best_score:
                    # print(path_f_score)
                    # print(best_score)
                    # print("Got it! F " + str(self.agv_id) + " ,node: " + str(node) + " ,to: " + str(closest_node))
                    best_score = path_f_score
                    opt_path = temp_path_f
            new_path = opt_path
        self.routes[0].path = new_path[:]
