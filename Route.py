#!/usr/bin/env python
class Route:

    def __init__(self, path, cost_map, init_score):
        self.path = path
        self.cost_map = cost_map
        self.init_score = init_score
        self.score = self.get_score()

    def update_score(self):
        self.score = self.get_score()

    def get_score(self):
        score = self.init_score
        for i in range(len(self.path) - 1):
            f_node = self.path[i]
            e_node = self.path[i + 1]
            # print("%d to %d: %f" % ( f_node, e_node, self.cost_map[f_node][e_node]))
            score += self.cost_map[f_node][e_node]
        # print("total: %f" % score)
        return score

    def get_inner_score(self):
        score = 0
        for i in range(len(self.path) - 1):
            f_node = self.path[i]
            e_node = self.path[i + 1]
            # print("%d to %d: %f" % ( f_node, e_node, self.cost_map[f_node][e_node]))
            score += self.cost_map[f_node][e_node]
        # print("total: %f" % score)

        return score
