#!/usr/bin/env python
import math

from Route import Route


class ScrambleProcessor:
    def __init__(self, cluster_sequence, agvs_position, nodes_position, cost_map, best_routes_group,
                 top_num=3, variation_max_time=5):
        self.variation_max_time = variation_max_time
        self.cluster_sequence = cluster_sequence
        self.nodes_position = nodes_position
        self.agvs_position = agvs_position
        self.cost_map = cost_map
        self.top_num = top_num
        self.best_routes_group = best_routes_group
        self.scramble_map = self._update_scramble_map()

    def get_all_group_best_score(self):
        return [routes.score for routes in self.best_routes_group]

    @staticmethod
    def _get_square_distance(position1, position2):
        x_2 = (position1[0] - position2[0]) ** 2
        y_2 = (position1[1] - position2[1]) ** 2
        result = math.sqrt(x_2 + y_2)
        return result

    def _update_scramble_map(self):
        node_num = len(self.cluster_sequence)
        agv_num = len(self.best_routes_group)
        scramble_map = [[0.0 for i in range(node_num)] for j in range(agv_num)]
        if node_num < 2:
            return scramble_map
        for i in range(node_num):
            distance_list = self.cost_map[i][:i] + self.cost_map[i][i + 1:]
            mean_distance = sum(distance_list) / (node_num - 1)
            select_node = []
            for j in range(node_num):
                if mean_distance >= self.cost_map[i][j] > 0:
                    select_node.append(j)
            select_node_num = len(select_node)
            if select_node_num == 0:
                continue
            for k in range(agv_num):
                counter = 0
                if k == self.cluster_sequence[i]:
                    scramble_map[k][i] = -1
                    continue
                else:
                    for l in select_node:
                        if self.cluster_sequence[l] == k:
                            counter += 1
                    scramble_map[k][i] = counter / select_node_num
        self.scramble_map = scramble_map
        # print("#########################")
        # for i in self.scramble_map:
        #     print(i)
        # print("#########################")
        return scramble_map

    def get_cluster_sequence(self):
        return self.cluster_sequence

    def _update_cluster_sequence(self):
        routes = [raa.path for raa in self.best_routes_group]
        for i, nodes in enumerate(routes):
            for j in nodes:
                self.cluster_sequence[j] = i

    def get_min_cost_agv(self):
        agv_id = -1
        cost = 999999
        for _id, routes in enumerate(self.best_routes_group):
            score_temp = routes.score
            if score_temp < cost:
                cost = score_temp
                agv_id = _id
        return agv_id, cost

    def get_max_cost_agv(self):
        agv_id = -1
        cost = -1
        for _id, routes in enumerate(self.best_routes_group):
            score_temp = routes.score
            if score_temp > cost:
                cost = score_temp
                agv_id = _id
        return agv_id, cost

    def get_scramble_node(self, from_agv, to_agv):
        filtered_node = []
        for i, value in enumerate(self.scramble_map[to_agv]):
            if value != 0.0 and self.scramble_map[from_agv][i] == -1:
                filtered_node.append([i, value])
        filtered_node = sorted(filtered_node, key=lambda data: data[1], reverse=True)
        if len(filtered_node) >= self.top_num:
            selected_scramble_node = list(list(zip(*filtered_node))[0])[:self.top_num]
        else:
            selected_scramble_node = []
        # print(filtered_node)
        # print("from_agv: " + str(from_agv))
        # print("to_agv: " + str(to_agv))
        # print(selected_scramble_node)
        return selected_scramble_node

    def get_closest_node_for_scramble_to_insert_agv_route(self, scramble_node, insert_agv_route, agv_position):
        scramble_node_position = self.nodes_position[scramble_node]
        new_init = self._get_square_distance(scramble_node_position, agv_position)
        node_id = -1
        shortest_distance = 999999
        for node in insert_agv_route.path:
            # insert_node_position = self.nodes_position[node]
            # distance = self._get_square_distance(scramble_node_position, insert_node_position)
            distance = self.cost_map[scramble_node][node]
            # if distance < shortest_distance and insert_agv_route.init_score < new_init:
            if distance < shortest_distance and distance < new_init:
                shortest_distance = distance
                node_id = node
        return node_id

    def scramble(self):
        if len(self.cluster_sequence) < self.top_num:
            return
        loop_time = self.variation_max_time
        ## loop until non-raise
        while loop_time > 0:
            ## 選出最小耗時及最大耗時的AGV
            ## 並取出原始路徑，以便下面複製運算
            min_cost_agv, _ = self.get_min_cost_agv()
            original_min_cost_agv_route = self.best_routes_group[min_cost_agv]
            # optimal_min_cost_agv_route = copy.deepcopy(original_min_cost_agv_route)
            optimal_min_cost_agv_route = Route(original_min_cost_agv_route.path[:],
                                               original_min_cost_agv_route.cost_map,
                                               original_min_cost_agv_route.init_score)
            max_cost_agv, max_cost = self.get_max_cost_agv()
            original_max_cost_agv_route = self.best_routes_group[max_cost_agv]
            # optimal_max_cost_agv_route = copy.deepcopy(original_max_cost_agv_route)
            optimal_max_cost_agv_route = Route(original_max_cost_agv_route.path[:],
                                               original_max_cost_agv_route.cost_map,
                                               original_max_cost_agv_route.init_score)
            optimal_cost = max_cost
            selected_scramble_node = self.get_scramble_node(max_cost_agv, min_cost_agv)
            if len(selected_scramble_node) == 0:
                break
            for scramble_node in selected_scramble_node:
                insert_node = self.get_closest_node_for_scramble_to_insert_agv_route(scramble_node,
                                                                                     original_min_cost_agv_route,
                                                                                     self.agvs_position[min_cost_agv])
                if insert_node < 0:
                    loop_time -= 1
                    continue
                insert_node_index = original_min_cost_agv_route.path.index(insert_node)
                scramble_node_index = original_max_cost_agv_route.path.index(scramble_node)
                # operator_max_cost_agv_route = copy.deepcopy(original_max_cost_agv_route)
                operator_max_cost_agv_route = Route(original_max_cost_agv_route.path[:],
                                                    original_max_cost_agv_route.cost_map,
                                                    original_max_cost_agv_route.init_score)
                # pop出scramble node，並更新score
                operator_max_cost_agv_route.path.pop(scramble_node_index)
                operator_max_cost_agv_route.update_score()
                ## 複製出兩個min_cost_agv_route，待會做前/後插入
                # operator_min_cost_agv_route_f = copy.deepcopy(original_min_cost_agv_route)
                # operator_min_cost_agv_route_b = copy.deepcopy(original_min_cost_agv_route)
                operator_min_cost_agv_route_f = Route(original_min_cost_agv_route.path[:],
                                                      original_min_cost_agv_route.cost_map,
                                                      original_min_cost_agv_route.init_score)
                operator_min_cost_agv_route_b = Route(original_min_cost_agv_route.path[:],
                                                      original_min_cost_agv_route.cost_map,
                                                      original_min_cost_agv_route.init_score)
                operator_min_cost_agv_route_f.path.insert(insert_node_index, scramble_node)
                # 若取代本起點，須更新agv到起點的init_score
                if insert_node_index == 0:
                    operator_min_cost_agv_route_f.init_score = self._get_square_distance(
                        self.agvs_position[min_cost_agv], self.nodes_position[scramble_node])
                operator_min_cost_agv_route_f.update_score()
                operator_min_cost_agv_route_b.path.insert(insert_node_index + 1, scramble_node)
                operator_min_cost_agv_route_b.update_score()
                # 插入最近點之後
                if max(operator_min_cost_agv_route_b.score, operator_max_cost_agv_route.score) < optimal_cost:
                    optimal_max_cost_agv_route = operator_max_cost_agv_route
                    optimal_min_cost_agv_route = operator_min_cost_agv_route_b
                    optimal_cost = max(operator_min_cost_agv_route_b.score, operator_max_cost_agv_route.score)
                # 插入最近點之前
                if max(operator_min_cost_agv_route_f.score, operator_max_cost_agv_route.score) < optimal_cost:
                    optimal_max_cost_agv_route = operator_max_cost_agv_route
                    optimal_min_cost_agv_route = operator_min_cost_agv_route_f
                    optimal_cost = max(operator_min_cost_agv_route_f.score, operator_max_cost_agv_route.score)
                # 檢查是否有提昇效率
                if optimal_cost < max_cost:
                    max_cost = optimal_cost
                    loop_time = self.variation_max_time
                    # print("max_cost:" + str(max_cost))
                else:
                    loop_time -= 1
            # print("loop_time:" + str(loop_time))
            self.best_routes_group[max_cost_agv] = optimal_max_cost_agv_route
            self.best_routes_group[min_cost_agv] = optimal_min_cost_agv_route
            self.optimal_by_swap_all_node(min_cost_agv)
            # print(self.cluster_sequence)
            self._update_cluster_sequence()
            # print("+++++-+-+-+-+-+-+-+-+-+")
            # print(self.cluster_sequence)
            self._update_scramble_map()
            yield max_cost
        return

    def get_closest_node(self, route_index, node):
        distance = 999999
        closest_node = -1
        for value in self.best_routes_group[route_index].path:
            if value == node:
                continue
            if self.cost_map[node][value] < distance:
                distance = self.cost_map[node][value]
                closest_node = value
        return closest_node

    def optimal_by_swap_all_node(self, route_index):
        best_route = self.best_routes_group[route_index]
        old_path = best_route.path[:]
        if len(old_path) < 2:
            return
        # print(old_path)
        best_score = best_route.get_score()
        new_path = old_path[:]
        for node in old_path:
            path_calculator_b = Route(old_path, best_route.cost_map, best_route.init_score)
            path_calculator_f = Route(old_path, best_route.cost_map, best_route.init_score)
            temp_path_f = new_path[:]
            temp_path_b = new_path[:]
            closest_node = self.get_closest_node(route_index, node)
            i = new_path.index(node)
            temp_path_b.pop(i)
            temp_path_f.pop(i)
            closest_node_index = new_path.index(closest_node)
            temp_path_b.insert(closest_node_index + 1, node)
            temp_path_f.insert(closest_node_index, node)
            # path_b score
            path_calculator_b.path = temp_path_b
            path_b_score = path_calculator_b.get_score()
            # path_f score
            path_calculator_f.path = temp_path_f
            # 檢查起點是否被替換，需要更新initial_score
            if closest_node_index == 0:
                # print("Replace begin node!")
                distance = self._get_square_distance(self.agvs_position[route_index], self.nodes_position[node])
                path_calculator_f.init_score = distance
            path_f_score = path_calculator_f.get_score()
            if path_b_score < best_score:
                # print(path_b_score)
                # print(best_score)
                # print("Got it! B " + str(self.agv_id) + " ,node: " + str(node) + " ,to: " + str(closest_node))
                best_score = path_b_score
                new_path = temp_path_b
            if path_f_score < best_score:
                # print(path_f_score)
                # print(best_score)
                # print("Got it! F " + str(self.agv_id) + " ,node: " + str(node) + " ,to: " + str(closest_node))
                best_score = path_f_score
                new_path = temp_path_f
        self.best_routes_group[route_index].path = new_path[:]
