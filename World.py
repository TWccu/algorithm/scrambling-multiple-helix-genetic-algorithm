#!/usr/bin/env python
from multiprocessing import Process, Pipe


class World:

    def __init__(self, assign_sequence, routes_group, cost_map, avgs_position, nodes_position, variation_max_time=5,
                 group_evolute_level=100,
                 use_multiple_process=False):
        self.nodes_position = nodes_position
        self.avgs_position = avgs_position
        self.group_evolute_level = group_evolute_level
        self.variation_max_time = variation_max_time
        self.cost_map = cost_map
        self.routes_group = routes_group
        self.variation_list = []  # 1 - 平均成長比例
        self.assign_sequence = assign_sequence
        self.score = self.get_score()
        self.use_multiple_process = use_multiple_process

    def get_all_group_best_score(self):
        return [routes.best_score for routes in self.routes_group]

    def get_score(self):
        score = 0
        for routes in self.routes_group:
            score = max(score, routes.get_best_score())
        return score

    def evolute_by_multiple_process(self):
        pipes = [Pipe() for _ in range(len(self.routes_group))]
        processes = []
        # s = datetime.datetime.now()
        for result_id, group in enumerate(self.routes_group):
            group.level = self.group_evolute_level
            p = Process(target=group.evolution_by_multiple_process,
                        args=(pipes[result_id][1], self.variation_max_time,))
            p.start()
            processes.append(p)
        for i, p in enumerate(processes):
            self.routes_group[i].routes = pipes[i][0].recv()
            p.join()
        # e = datetime.datetime.now()
        # print((e - s).microseconds)

    def evolute_by_single_process(self):
        # s = datetime.datetime.now()
        for group in self.routes_group:
            group.level = self.group_evolute_level
            group.evolution(self.variation_max_time)
        # e = datetime.datetime.now()
        # print((e - s).microseconds)

    def evolute(self):
        variation_counter = self.variation_max_time
        while True:
            if self.use_multiple_process:
                self.evolute_by_multiple_process()
            else:
                self.evolute_by_single_process()
            score = self.get_score()

            if score == self.score:
                variation_counter -= 1
                if variation_counter == 1:
                    # print('swap_all_node')

                    for group in self.routes_group:
                        group.optimal_by_swap_all_node(self.avgs_position, self.nodes_position)
                    score = self.get_score()

                    # print("new_score: " + str(score))
                    # print("old_score: " + str(self.score))
                    # print(self.routes_group[0].routes[0].path)
                    # print(self.routes_group[1].routes[0].path)
                    # print(self.routes_group[2].routes[0].path)
                    if score == self.score:
                        return
                    else:
                        variation_counter = self.variation_max_time
                        self.score = score
                        yield score
            else:
                variation_counter = self.variation_max_time
                self.score = score
                yield score
